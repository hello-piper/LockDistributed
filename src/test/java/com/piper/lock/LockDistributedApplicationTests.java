package com.piper.lock;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.Jedis;

import java.util.Date;

@SpringBootTest
class LockDistributedApplicationTests {

    @Autowired
    private RedisTemplateUtil redisTemplateUtil;

    @Test
    void testLock() throws Exception {
        String key = "key";
        String value = "value";

        Boolean lock = redisTemplateUtil.lock(key, value, 30L);
        Thread.sleep(5000);
        Boolean unlock = redisTemplateUtil.unlock(key, value);

        System.out.println("加锁结果:" + lock);
        System.out.println("删除锁结果:" + unlock);
        System.out.println(String.format("执行结束:%tT", new Date()));
        Thread.sleep(3000);
    }

    @Test
    void testJedisLock() throws Exception {
        String key = "key";
        String value = "value";

        Jedis jedis = new Jedis("localhost", 6379);
        jedis.select(0);

        boolean lock = JedisLock.lock(jedis, key, value, 30);
        Thread.sleep(5000);
        boolean unLock = JedisLock.unLock(jedis, key, value);

        System.out.println("加锁结果:" + lock);
        System.out.println("删除锁结果:" + unLock);
        System.out.println(String.format("执行结束:%tT", new Date()));
        Thread.sleep(3000);
    }

}
