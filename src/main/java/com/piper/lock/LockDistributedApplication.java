package com.piper.lock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LockDistributedApplication {

	public static void main(String[] args) {
		SpringApplication.run(LockDistributedApplication.class, args);
	}

}
