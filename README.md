# LockDistributed

### 一 介绍
我为什么要写分布式锁呢，最近工作中写一个查询接口，因为逻辑复杂，不希望用户不停的点击，需要过滤掉重复的请求。
简单的需求：用户每3秒只能请求一次，否则拒绝。

这个实现很简单。用户第一次请求在redis记下标记，设置3秒过期时间，下次用户再请求判断标记是否过期，没过期就拒绝请求，过期了，就重新设置标记。

后来我在做加锁这一块的时候，脑袋里出来一个想法：**不如写一套分布式锁吧**！

### 二 加锁
#### 1 SETNX
```SETNX key value```当不存在key时设置，相当于SET命令，否则不做任何操作。

```java
--- setIfAbsent函数，当不存在时设置成功（相当于redis命令setnx）
Boolean absent = stringRedisTemplate.opsForValue().setIfAbsent(key, "");
if (absent) {
     stringRedisTemplate.expire(key, 3L, TimeUnit.SECONDS);
} else {
	return "请求频繁，请稍后再试。";
}
```
上边代码能够实现加锁，但是有两个问题需要改进：
- set 和 expire 需要发送两次请求，无形中增加了连接损耗
- set 和 expire 不是原子操作，如果set成功后系统报错，就会造成死锁，给系统增加了隐患。

针对这两个问题，引出下一节set命令的讲解。

#### 2 SET NX EX
```SET key value [EX seconds] [PX milliseconds] [NX|XX]```

从 Redis 2.6.12 版本开始， SET 命令的行为可以通过一系列参数来修改：
- EX seconds-设置指定的终止时间，以秒为单位。
- PX 毫秒 -设置指定的到期时间（以毫秒为单位）。
- NX -仅设置不存在的密钥。
- XX -仅设置密钥（如果已存在）。

注意：由于SET命令选项可以替换SETNX，SETEX，PSETEX，因此在Redis的未来版本中，这三个命令可能会被弃用并最终删除。

介绍我最终使用的命令：```SET key value NX EX second```
该命令是使用Redis实现锁定系统的简单方法。当key不存在时设置key为value并设置过期时间为second。

注意，value应该使用随机值，不应该使用固定数据。这样可以避免客户端在到期时间之后尝试删除该锁，但是删除了后面获得该锁的另一个客户端创建的锁。

### 三 解锁
#### 3.1 EVAL
```EVAL script numkeys key [key ...] arg [arg ...]·```

eval命令用来执行lua脚本，因为lua语言非常精小，redis内置了对lua语言的支持，redis原子执行Lua脚本。
- script ：用户编写的一段lua脚本
- numkeys ：传入的KEYS参数数量
- key ：键，可以有多个
- args ：ARGS参数，可以有多个

快速入门：以下命令展示了eval如何使用以及通常后面脚本中使用的元素
```java
> eval "return redis.call('set',KEYS[1],ARGV[1])" 1 key value
OK
```
以上有三点需要了解：
- redis.call() ： Lua脚本中调用redis命令
- KEYS[1] ：Lua使用KEYS访问全局变量，从1开始
- ARGV[1] ：其他参数不应该代表键名称，可以通过Lua来访问ARGV全局变量，与键非常相似。

#### 3.2 解锁脚本
有了第一节的知识铺垫，我这里就直接抛出解锁脚本。
```java
if redis.call("get",KEYS[1]) == ARGV[1]
then
    return redis.call("del",KEYS[1])
else
    return 0
end
```
上述脚本，获取key是否等于value，如果一致，说明这把锁是我自己加的，那么就删除它，否则解锁失败。
该脚本应使用 EVAL script 1 key value来使用。

### 四 看门狗🐶
上述加锁解锁脚本都已经完成，已经具备了分布式锁的外观！可是，别急，这里还有一个问题。

如果我程序执行时间很长，超过了加锁时长，那么等锁过期后，其他线程就会加锁成功，业务就会出问题。

我翻阅了Redisson分布式锁源码，发现Redisson内部有一个Watch Dog的概念，加锁成功后，启动一个线程每隔10秒会检查锁是否还存在，还存在的话就重新设置为30秒，这叫定时续约。

依照这一思路，我实现了自己的简易版Watch Dog程序。

首先需要解决两个问题：
- 需要一个线程，跟随主线程，当主线程销毁后，它也会跟着销毁，这就需要我们的Daemon Thread啦。
- 需要定时执行，幸运的是，Java提供了Timer和ScheduledExecutorService提供给我们做定时任务。

具体实现是，加锁成功后，创建Daemon Thread放到Timer，设置执行频率，就可以实现我们的看门狗啦！

本文源码在：[https://gitee.com/hello-piper/LockDistributed](https://gitee.com/hello-piper/LockDistributed) ，如果有用就给我一个Star吧！

如果大家觉得有用，可以点赞、评论、收藏支持我哦！
